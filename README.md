# Email Tool – Frontend Website Development

To complete this challenge you should build a **responsive website frontend** of an Email tool plataform.

## Instructions

1 - Clone or download the code skeleton on git@bitbucket.org:AngryVenturesInc/email-tool-frontend-challenge.git .

2 – On the ***mockups*** folder you will find the mockups that you should use to build the frontend.

3 – On the ***images*** folder you will found the background.

4 – On the ***icones*** folder you will found the icons that appear on mockups.

5 - After building the differents views, you should send your resolution to hi@angryventures.com (zip or your own git repository).
   The email subject should be "YOUR NAME_Frontend Challenge"

If we need any additional commands to test your resolutions, please explain them in the e-mail.


### If you’d like to bring any more features or improvements to the table, feel free to do it!
